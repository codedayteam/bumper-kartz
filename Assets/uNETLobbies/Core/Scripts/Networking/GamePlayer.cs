﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class GamePlayer : NetworkBehaviour {

    [SyncVar(hook = "OnNameChange")]
    public string playerName;

    [SyncVar(hook = "OnColorChange")]
    public Color playerColor;

    [SyncVar(hook = "OnInvincibleStateChange")]
    public bool Invincible = false;

    public PInfoHolder PHolder;
    public PInfoHolder PHolderPrefab;

    public void Start() {
        PHolder = (PInfoHolder)GameObject.Instantiate(PHolderPrefab, new Vector3(0, 0, 0), Quaternion.identity);
        PHolder.gameObject.transform.SetParent(GameObject.Find("Canvas").transform.Find("Players"), false);
        PHolder.PlayerName.text = playerName;
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        NetworkLobby.s_Singleton.localPlayer = gameObject;
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (playerName != "")
        {
            DestroyLobbyCounterpart();
            gameObject.name = playerName;
        }
    }

    protected void DestroyLobbyCounterpart()
    {
        Debug.Log("Destroying lobby counterpart of " + playerName);
        GameObject go = GameObject.Find(playerName);
        LobbyPlayer lp = go.GetComponent<LobbyPlayer>();
        if (lp != null)
        {
            GameObject.Destroy(go);
        }
    }

    #region Name Functions
    public void ServerSetPlayerName(string s)
    {
        playerName = s;
    }

    [Command]
    void Cmd_ChangeName(string s)
    {
        playerName = s;
    }

    void OnNameChange(string s)
    {
        playerName = s;
        DestroyLobbyCounterpart();
        gameObject.name = s;
    }
    #endregion

    #region Colors
    public void ServerSetPlayerColor(Color s) {
        playerColor = s;
    }

    [Command]
    public void Cmd_ChangeColor(Color s) {
        playerColor = s;
    }

    public void OnColorChange(Color s) {
        playerColor = s;
    }
    #endregion

    [Command]
    public void Cmd_ChangeInvincibleState(bool s) {
        Invincible = s;
    }

    public void OnInvincibleStateChange(bool s) {
        Invincible = s;
    }
}
