﻿using UnityEngine;
using System.Collections;

public class GameMenu : MonoBehaviour {
    NetworkLobby networkLobby;
	// Use this for initialization
	void Start () {

	}

    public void LeaveGame()
    {
        Debug.Log("Leaving game.");
        NetworkLobby.s_Singleton.LeaveGame();
    }
}
