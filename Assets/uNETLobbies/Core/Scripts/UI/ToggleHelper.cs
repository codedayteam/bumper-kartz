﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ToggleHelper : MonoBehaviour {
    UnityEngine.UI.Toggle toggle;

    [SerializeField]
    GameObject enabledLayer;

    [SerializeField]
    GameObject disabledLayer;

    
	void Awake () {
        Debug.Log("**** TOGGLE STARTED ****");
        toggle = GetComponent<UnityEngine.UI.Toggle>();
        //OnToggleToggle();
	}
	
    public void SetInteractable(bool b)
    {
        Debug.Log("About to set interactable");
        toggle.interactable = b;
        Debug.Log("Just set interactable");
        OnToggleInteractable();
    }
    

    public void OnToggleInteractable()
    {
        if (toggle.interactable)
        {
            if (enabledLayer)
            {
                enabledLayer.SetActive(true);
            }
            if (disabledLayer)
            {
                disabledLayer.SetActive(false);
            }
        }
        else
        {
            if (enabledLayer)
            {
                enabledLayer.SetActive(false);
            }
            if (disabledLayer)
            {
                disabledLayer.SetActive(true);
            }
        }
    }
    /*
    public void OnToggleToggle()
    {
        
        if (toggle.isOn)
        {
            if (enabledLayer)
            {
                enabledLayer.SetActive(true);
            }
            if (disabledLayer)
            {
                disabledLayer.SetActive(false);
            }
        }
        else
        {
            if (enabledLayer)
            {
                enabledLayer.SetActive(false);
            }
            if (disabledLayer)
            {
                disabledLayer.SetActive(true);
            }
        }
    }*/
}
