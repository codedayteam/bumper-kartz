﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class BPBattle : NetworkBehaviour{

    private GamePlayer _GP;
    private Rigidbody2D _Rigid;
    [SyncVar(hook = "OnScoreChange")]
    public int Score = 0;
    [SyncVar(hook ="OnPercentChange")]
    public float Percentage = 0;
    public float ForceMultiplier = 10;
    public float OtherMultiplier = .75f;
    //Colors!
    public SpriteRenderer SpriteRender;
    //Other stuff
    [HideInInspector]
    public BPBattle LastGOHitBy;

    public void Awake() {
        _GP = GetComponent<GamePlayer>();
        _Rigid = GetComponent<Rigidbody2D>();
    }

    public void Start() {
        if (isLocalPlayer) {
            _GP.Cmd_ChangeColor(_GP.playerColor);
        }
        SpriteRender.color = _GP.playerColor;
    }

    public void Update() {
        SpriteRender.color = _GP.Invincible ? new Color(1, 1, 1, .5f) : _GP.playerColor;
        _GP.PHolder.Percentage.text = Percentage.ToString("F0") + "%";
        _GP.PHolder.CurrentScore.text = "+" + Score.ToString(); 
    }

    public void OnTriggerEnter2D(Collider2D other) {
        //If we're not invincible...
        if (isLocalPlayer) {
            if (!_GP.Invincible) {
                if (other.tag == "Player") {
                    //If the other player isn't invincible...
                    if (!other.GetComponent<GamePlayer>().Invincible) {
                        if (_Rigid.velocity.magnitude > other.GetComponent<Rigidbody2D>().velocity.magnitude) {
                            //Calculate damage given
                            float DamageToGive = _Rigid.velocity.magnitude * 2;
                            Vector2 OppositeDirection = (other.transform.position - transform.position).normalized;
                            other.GetComponent<BPBattle>().CmdTakeDamage(DamageToGive, OppositeDirection);
                            LastGOHitBy = other.GetComponent<BPBattle>();
                        }
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Stage") {
            StopCoroutine("SpawnInvincibility");
            //Give the last player we where hit by a point
            if (LastGOHitBy != null) {
                LastGOHitBy.CmdChangeScore(LastGOHitBy.Score + 1);
                LastGOHitBy = null;
            }
            //Reset our variables
            GetComponent<BPBattle>().Percentage = 0;
            GetComponent<BoxCollider2D>().enabled = false;
            transform.position = new Vector3(0, 0, 0);
            _Rigid.velocity = Vector2.zero;
            GetComponent<BoxCollider2D>().enabled = true;
            //Invincible for 5 seconds when we spawn
            StartCoroutine("SpawnInvincibility");
        }
    }

    IEnumerator SpawnInvincibility() {
        _GP.Cmd_ChangeInvincibleState(true);
        yield return new WaitForSeconds(5);
        _GP.Cmd_ChangeInvincibleState(false);
        SpriteRender.color = _GP.playerColor;
    }

    //NETWORKING//
    [Command]
    public void CmdTakeDamage(float Damage, Vector2 Direction) {
        Percentage += Damage;
        //_Rigid.AddForce((Direction * (Damage + Percentage))*ForceMultiplier);
        RpcTestThing(((Direction * (Damage + Percentage)) * ForceMultiplier));
    }

    [ClientRpc]
    public void RpcTestThing(Vector2 ForceTo) {
        Debug.Log("HAI");
        _Rigid.AddForce(ForceTo);
    }

    //Score
    [Command]
    public void CmdChangeScore(int _NewScore) {
        Score = _NewScore;
    }

    public void OnScoreChange(int _NewScore) {
        Score = _NewScore;
    }

    //Percent
    [Command]
    public void CmdChangePercent(float _Percent) {
        Percentage = _Percent;
    }

    public void OnPercentChange(float _Percent) {
        Percentage = _Percent;
    }
}
