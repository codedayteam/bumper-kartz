﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Rewired;

public class BPInput : NetworkBehaviour{

    private Player _Player;
    private float ForwardForce;
    private float RotateForce;
    [HideInInspector]
    public BPMovement _BPMove;
    

	void Awake () {
        _BPMove = GetComponent<BPMovement>();
        _Player = ReInput.players.GetPlayer(0);
	}

	void Update () {
        if (!isLocalPlayer) {
            return;
        }
    }

    void FixedUpdate() {
        if (!isLocalPlayer) {
            return;
        }
        ForwardForce = _Player.GetAxis("Left Stick Y");
        RotateForce = _Player.GetAxis("Right Stick X");

        _BPMove.Movement(ForwardForce, RotateForce);
    }
}
