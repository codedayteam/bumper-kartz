﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using Rewired;

public class BPMovement : NetworkBehaviour {

    private GamePlayer _GP;
    private AudioSource _ASource;
    private Rigidbody2D _Rigid;
    public float Acceleration = 1;
    public float MaxSpeed = 10;
    public float TurnSpeed = 5;
    //Audio
    public AudioClip IdleSound;
    public AudioClip MovingSound;
    public float multiplier = 10;


    public void Awake() {
        _GP = GetComponent<GamePlayer>();
        _ASource = GetComponent<AudioSource>();
        _Rigid = GetComponent<Rigidbody2D>();
    }

    void Update() {
        //Sound
        if(_Rigid.velocity.magnitude < 1 && _Rigid.velocity.magnitude > -1 && _ASource.clip == MovingSound) {
            _ASource.volume = 1;
            _ASource.clip = IdleSound;
            _ASource.Play();
        }else if(_Rigid.velocity.magnitude > 1 && _Rigid.velocity.magnitude < -1 && _ASource.clip == IdleSound) {
            _ASource.clip = MovingSound;
            _ASource.Play();
        }
    }

    public void Movement(float _MoveForce, float _TurnForce) {
        //Movement
        _Rigid.AddForce(transform.up * _MoveForce * Acceleration * Time.deltaTime);
        //Limit Speed
        if(_Rigid.velocity.magnitude > MaxSpeed) {
            _Rigid.velocity = _Rigid.velocity.normalized * MaxSpeed;
        }
        //Rotation
        transform.Rotate(-Vector3.forward, TurnSpeed * _TurnForce);

        //Sound
        if (_MoveForce != 0) {
            _ASource.volume = .25f + ((_Rigid.velocity.magnitude / MaxSpeed) * multiplier);
        }
    }
}