﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class MatchmakingHandler : NetworkBehaviour {

    [HideInInspector]
    public List<SInfoHolder> SInfoList = new List<SInfoHolder>();
    public GameObject ServerInfoPrefab;
    public GameObject SInfoContentGO;
    //Password Stuff
    public Text Password;
    public Button EnterB;

    void OnEnable() {
        InvokeRepeating("FillServerList", 0, 10);
    }

    public void FillServerList() {
        for (int i = 0; i < SInfoList.Count; i++) {
            Destroy(SInfoList[i].gameObject);
        }
        SInfoList.Clear();

        NetworkLobby.s_Singleton.matchMaker.ListMatches(0, 20, "", OnMatchList);
    }

    public void OnMatchList(ListMatchResponse matchListResponse) {
        if (matchListResponse.success && matchListResponse.matches != null) {
            //If we got the list and the list isn't empty
            int CurrentIndex = 0;
            foreach (MatchDesc mD in matchListResponse.matches) {
                GameObject tempSI = (GameObject)GameObject.Instantiate(ServerInfoPrefab.gameObject, new Vector3(0, 0, 0), Quaternion.identity);
                tempSI.transform.SetParent(SInfoContentGO.transform);
                tempSI.transform.localScale = new Vector3(1, 1, 1);
                SInfoHolder SH = tempSI.GetComponent<SInfoHolder>();
                SH.MatchD = mD;
                SH.ServerName.text = mD.name;
                SH.PlayerCount.text = mD.maxSize.ToString();
                SInfoList.Add(SH);
                CurrentIndex++;
            }
        }
    }
}
