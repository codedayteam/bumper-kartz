﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using System.Collections;

public class SInfoHolder : MonoBehaviour {

    public JoinMatchResponse MyRoomInfo;
    public MatchDesc MatchD;
    public Text ServerName;
    public Text PlayerCount;

    public void JoinTheRoom() {
        LobbyMenu.Current.MHandler.MessageText.text = "Joining lobby...";
        LobbyMenu.Current.MHandler.gameObject.SetActive(true);
        NetworkLobby.s_Singleton.matchMaker.JoinMatch(MatchD.networkId, "", NetworkLobby.s_Singleton.OnMatchJoined);
    }
}
