﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

public class PInfoHolder :  NetworkBehaviour{

    public Text PlayerName;
    public Text Percentage;
    public Text CurrentScore;
}
